<?php

use App\Http\Controllers\Api\v1\TicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('open-tickets', [TicketController::class, 'getOpenTickets']);
Route::get('closed-tickets', [TicketController::class, 'getClosedTickets']);
Route::get('users/{email}/tickets', [TicketController::class, 'getTicketsByUser']);
Route::get('stats', [TicketController::class, 'getStats']);
