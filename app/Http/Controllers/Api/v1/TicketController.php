<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\TicketResource;
use App\Services\Contracts\TicketServiceContract;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Collection;

class TicketController extends Controller
{

    private TicketServiceContract $ticketService;

    /**
     * TicketController constructor.
     * @param TicketServiceContract $ticketService
     */
    public function __construct(TicketServiceContract $ticketService)
    {
        $this->ticketService = $ticketService;
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function getOpenTickets(): AnonymousResourceCollection
    {
        return TicketResource::collection($this->ticketService->getOpenTickets());
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function getClosedTickets(): AnonymousResourceCollection
    {
        return TicketResource::collection($this->ticketService->getClosedTickets());
    }

    /**
     * @param $email
     * @return AnonymousResourceCollection
     */
    public function getTicketsByUser($email): AnonymousResourceCollection
    {
        return TicketResource::collection($this->ticketService->getTicketByUser($email));
    }

    /**
     * @return Collection
     */
    public function getStats(): Collection
    {
        return $this->ticketService->getStats();
    }
}
