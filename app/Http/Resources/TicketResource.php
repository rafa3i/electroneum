<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'subject' => $this->subject,
            'content' => $this->content,
            'name' => $this->name,
            'email' => $this->email,
            'added' => $this->created_at,
            'status' => $this->status,
        ];
    }
}
