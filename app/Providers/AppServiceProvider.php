<?php

namespace App\Providers;

use App\Services\Contracts\TicketServiceContract;
use App\Services\TicketService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(TicketServiceContract::class, function($app) {
            return new TicketService();
        });
    }
}
