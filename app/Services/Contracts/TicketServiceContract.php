<?php

namespace App\Services\Contracts;

interface TicketServiceContract
{
    public function getOpenTickets();
    public function getClosedTickets();
    public function getTicketByUser(string $email);
    public function getStats();
}
