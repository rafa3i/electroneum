<?php

namespace App\Services;

use App\Models\Ticket;
use App\Models\User;
use App\Services\Contracts\TicketServiceContract;
use Illuminate\Database\Eloquent\Collection;

class TicketService implements TicketServiceContract
{

    /**
     * @return Collection
     */
    public function getOpenTickets()
    {
        return Ticket::where('status', '=', '0')->simplePaginate(10);
    }

    /**
     * @return Collection
     */
    public function getClosedTickets()
    {
        return Ticket::where('status', '=', '1')->simplePaginate(10);
    }

    /**
     * @return Collection
     */
    public function getTicketByUser(string $email)
    {
        return Ticket::with('user')
            ->where('email', '=', $email)
            ->get();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getStats()
    {
        $totalTickets = Ticket::all()
            ->count();

        $unprocessedTickets = Ticket::where('status', '=', 0)
            ->get()
            ->count();

        $userWithTickets = User::select(['name', 'email'])
            ->withCount('tickets')
            ->orderBy('tickets_count')
            ->limit(1)
            ->get();

        $lastProcessedTicket = Ticket::where('status', '=', 1)
            ->select(['subject', 'content', 'name', 'email', 'status', 'processed_at'])
            ->orderBy('processed_at', 'desc')
            ->first();

        return collect([
            'total' => $totalTickets,
            'unprocessed' => $unprocessedTickets,
            'user_with_most_tickets' => $userWithTickets,
            'last_processed_ticket' => $lastProcessedTicket,
        ]);
    }
}
