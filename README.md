## Electroneum Tech Test

Please run `sh start.sh` and the repo will be setup automatically and ready to go

In addition to the login functionlity requested which was a very quick job due to Laravel Jetstream,
have added some api functionality. The endpoinds are in routes/api.php and for the sake of simplicity, these are

`open-tickets`

`closed-tickets`

`users/{email}/tickets`

`stats`

The `sh start.sh` command also triggers a `php artisan migrate:fresh --seed` therefore there the database will already be populated with 250 tickets

In terms of the functionality, the `TicketController` uses `TicketService` which implements
`TicketServiceContracted` which in turn is initialized through the `AppServiceProvider`

There is also functionality to create `php artisan ticket:create` and process `php artisan ticket:process`

Any questions, please let me know
